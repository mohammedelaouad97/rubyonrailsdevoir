class UtilisateursController < ApplicationController
  def index
    @utilisateurs = Utilisateur.all
  end

  def show
    @utilisateurs = Utilisateur.find(params[:id])
  end
 
  def new
    @utilisateurs = Utilisateur.new
  end

  def create
    @utilisateurs = Utilisateur.new(utilisateur_params)

    if @utilisateurs.save
      redirect_to @utilisateurs
    else
      render :new, status: :unprocessable_entity
    end
  end

  private
  def utilisateur_params
    params.require(:utilisateur).permit(
      :nom,
      :prenom,
      :age,
      :adresse,
      :photo
    )
  end
  
  def edit
    @utilisateurs = Utilisateur.find_by(id: params[:id])
  end

  def update
    @utilisateurs = Utilisateur.find_by(id: params[:id])

    if @utilisateurs.update(utilisateur_params)
      flash[:notice] = "Bien modifier"
      redirect_to @utilisateurs
    else
      render :edit
    end
  end

  def delete
    @utilisateurs = Utilisateur.find(params[:id])
    @utilisateurs.destroy
    @utilisateurs = Utilisateur.all
    redirect_to "index"
  end
end